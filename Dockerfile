# Use an official Python runtime as a parent image
FROM python:3.7

# Set the working directory to /src
WORKDIR /.
RUN apt-get update

RUN pip install mysql-connector-python
RUN pip install flake8 pytest
RUN python -m pip install --upgrade pylint setuptools pip wheel
RUN pip install pandas
RUN pip install twine
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y git
RUN pip install py3-validate-email


# Copy the current directory contents into the container at /src


# Run app.py when the container launches
CMD ["python", "app.py"]
