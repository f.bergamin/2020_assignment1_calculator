"""
Funzione di registrazione utente
"""
import sys
import db
import calcolatrice
import accesso
from validate_email import validate_email


def registrazione(email, password):
    """
    Funzione registrazione utente.
    La funzione controlla lo stato della connessione,
    dopodichè crea la tabella user_list (se non ancora
    esistente).
    La registrazione avviene a seguito di un controllo
    che verifica la validità dell'email.
    """
    if db.connessione():
        db.create_db()
        if db.insert(email, password):
            print("Registrazione avvenuta")
            return True
        elif not db.insert(email, password):
            print("Impossibile effettuare la registrazione")
            return False
    else:
        print("Impossibile connettersi")
        return False


def login(email, password):
    """
    Funzione di login utente
    La funzione controlla lo stato della connessione,
    dopodichè crea la tabella user_list (se non ancora
    esistente).
    Il login  avviene a seguito di un controllo
    che verifica la validità delle credenziali inserite.
    In caso di non validità delle credenziali restituisce errore.
    """
    if db.connessione():
        db.create_db()
        if db.check_password(email, password):
            print("Login eseguito con successo")
            return True
        else:
            print("E-mail o password non corretti")
            return False
    else:
        print("Impossibile connettersi")
        return False


def interfaccia(email):
    """
    Funzione interfaccia.
    Implementa la logica di funzionamento della calcolatrice.
    Viene richiamata in seguito ad un login eseguito con successo.
    """
    operazione = ""
    print("Benvenuto" + '\t' + email)
    while(operazione != "q"):
        print("Premi uno dei seguenti comandi per eseguire un'operazione:")
        print("-  a per effettuare una addizione;" + '\n' +
              "-  s per effettuare una sottrazione;" + '\n' +
              "-  m per effettuare una moltiplicazione;" + '\n' +
              "-  d per effettuare una divisione" + '\n' +
              "-  q per uscire dal programma")
        operazione = input("Inserisci un comando:" + '\t')
        while(operazione != "a" and operazione != "s" and operazione != "m" and
              operazione != "d" and operazione != "q"):
            print("Comando non valido")
            operazione = input("Inserisci un comando:" + '\t')
        if(operazione == "q"):
            print("Arrivederci!" + '\n')
            sys.exit()
        num1 = input("Inserisci il primo numero." + '\t')
        # Tramite la funzione replace viene elaborata la stringa
        # in un formato che possa essere utilizzato per controllare
        # che il carattere inserito sia un valore numerico, eliminando
        # segni di punteggiatura e sostituendo il carattere ',' con il
        # carattere '.'.
        # Viene utilizzata la variabile temporanea 'temp' per eseguire
        # il controllo.
        num1 = num1.replace(",", ".")
        temp = num1.replace(".", "")
        temp = temp.replace("-", "")
        temp = temp.replace("+", "")
        while not temp.isdigit():
            print("Devi inserire un numero, non un carattere!")
            num1 = input("Inserisci il primo numero" + '\t')
            num1 = num1.replace(",", ".")
            temp = num1.replace(".", "")
            temp = temp.replace("-", "")
            temp = temp.replace("+", "")
        num2 = input("Inserisci il secondo numero." + '\t')
        # Tramite la funzione replace viene elaborata la stringa
        # in un formato che possa essere utilizzato per controllare
        # che il carattere inserito sia un valore numerico, eliminando
        # segni di punteggiatura e sostituendo il carattere ',' con il
        # carattere '.'.
        # Viene utilizzata la variabile temporanea 'temp' per eseguire
        # il controllo.
        num2 = num2.replace(",", ".")
        temp = num2.replace(".", "")
        temp = temp.replace("-", "")
        temp = temp.replace("+", "")
        while not temp.isdigit():
            print("Devi inserire un numero, non un carattere!")
            num2 = input("Inserisci il secondo numero" + '\t')
            num2 = num2.replace(",", ".")
            temp = num2.replace(".", "")
            temp = temp.replace("-", "")
            temp = temp.replace("+", "")

        if operazione == "a":
            print("Il risultato è:" +
                  '\t' + calcolatrice.add(num1, num2) + '\n')
        elif operazione == "s":
            print("Il risultato è:" +
                  '\t' + calcolatrice.subtract(num1, num2) + '\n')
        elif operazione == "m":
            print("Il risultato è:" +
                  '\t' + calcolatrice.multiplication(num1, num2) + '\n')
        elif operazione == "d":
            print("Il risultato è:" +
                  '\t' + calcolatrice.division(num1, num2) + '\n')
        elif operazione == "q":
            sys.exit()
        print("Premi q per uscire dal programma")
        print("Premi un qualsiasi altro carattere per proseguire")
        if(input("Inserisci carattere" + '\t') == "q"):
            print("Arrivederci!" + '\n')
            sys.exit()


if __name__ == "__main__":
    """
    Main.
    Gestisce l'avvio del programma.
    Crea all'avvio la tabella user_list (se non esistente) e permette
    di eseguire la scelta tra:
        - Nuovo utente --> y
        - Utente registrato --> n
        - Uscita --> q
    Esegue un controllo sull'esistenza del dominio dell'email inserita.
    """
    scelta = ""
    conn = db.init_conn()
    db.create_db()
    conn.close()

    while scelta not in ('y', 'n', 'q'):
        scelta = accesso.menu()
        if scelta not in ('y', 'n', 'q'):
            print("Error! Scelta non valida!")

    if scelta == "q":
        sys.exit()

    if scelta == "y":

        controllo = True
        while(controllo):
            email = accesso.input_email()
            password = accesso.input_password()
            is_valid = validate_email(email_address=email,
                                      check_regex=True, check_mx=True,
                                      from_address='my@from.addr.ess',
                                      helo_host='my.host.name', smtp_timeout=1,
                                      dns_timeout=1, use_blacklist=True,
                                      debug=False)
            if not is_valid:
                print("Email non valida!")
                controllo = True
            else:
                controllo = False
        if registrazione(email, password):
            scelta = input("Vuoi fare il login ? y/n")
            if scelta == "y":
                email = accesso.input_email()
                password = accesso.input_password()
                if login(email, password):
                    interfaccia(email)
            else:
                print("Arrivederci!")
                sys.exit()
        else:
            print("Errore di connessione")
            sys.exit()
    if scelta == "n":
        email = accesso.input_email()
        password = accesso.input_password()
        if login(email, password):
            interfaccia(email)
        else:
            sys.exit()
