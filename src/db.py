"""
Funzioni db
"""
import sqlite3


def init_conn():
    """
    Funzione per iniziare la connessione con il db
    """
    return sqlite3.connect("users.db")


def connessione():
    """
    Funzione per controllare la connessione con il db
    """
    conn = init_conn()
    cur = conn.cursor()
    res = cur.connection == conn
    conn.close()
    return res


def create_db():
    """
    Funzione per inizializzare il db se non esiste la tabella user_list
    """
    conn = init_conn()
    try:
        conn.execute("CREATE TABLE IF NOT EXISTS `user_list` " +
                     "(`email` varchar(23) PRIMARY KEY NOT NULL " +
                     "UNIQUE, `password` varchar(23) DEFAULT NULL);")
    except sqlite3.Error:
        conn.rollback()
        conn.close()
        return False
    conn.commit()
    conn.close()
    return True


def insert(email, password):
    """
    Funzione per inserire un record nel db
    """
    conn = init_conn()
    cur = conn.cursor()
    sqlite_insert = "INSERT INTO user_list  (`email`, `password`) VALUES (?,?)"
    try:
        cur.execute(sqlite_insert, (email, password))
    except sqlite3.Error:
        conn.rollback()
        conn.close()
        print("Nome utente già esistente")
        return False
    conn.commit()
    conn.close()
    return True


def select(email):
    """
    Funzione per restituire un record tramite email
    """
    conn = init_conn()
    cur = conn.cursor()
    sql = "SELECT * FROM user_list WHERE `email` = ?"
    try:
        cur.execute(sql, (email, ))
    except sqlite3.Error:
        conn.rollback()
        conn.close()
        return False
    res = cur.fetchone()
    conn.commit()
    conn.close()
    return res


def check_password(email, password):
    """
    Funzione per controllare che la password inserita
    corrisponda a quella del db
    """
    conn = init_conn()
    cur = conn.cursor()
    sql = "SELECT * FROM user_list WHERE (`email`,`password`) = (?,?) "
    try:
        sql = cur.execute(sql, (email, password, ))
        if len(cur.fetchall()) == 1:
            conn.close()
            return True
        else:
            conn.close()
            return False
    except sqlite3.Error:
        conn.rollback()
        conn.close()
        print("Errore connessione")
        return False
    conn.commit()
    conn.close()
    return True


def delete_user(email, password):
    """
    Funzione per l'eliminazione di utenti dal database
    """
    conn = init_conn()
    cur = conn.cursor()
    sql = "DELETE FROM user_list WHERE (`email`,`password`) = (?,?)"
    try:
        sql = cur.execute(sql, (email, password, ))
        sql = "SELECT * FROM user_list WHERE (`email`,`password`) = (?,?) "
        sql = cur.execute(sql, (email, password, ))
        if len(cur.fetchall()) == 1:
            conn.close()
            return False
        else:
            conn.close()
            print("utente cancellato")
            return True
    except sqlite3.Error:
        conn.rollback()
        conn.close()
        print("Errore connessione")
        return False
    conn.commit()
    conn.close()
    return True
