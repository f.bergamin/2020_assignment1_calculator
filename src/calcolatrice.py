"""
Calcolatrice
"""


def add(first_term, second_term):
    """
    Funzione addizione
    """
    result = float(first_term) + float(second_term)
    return str(result)


def subtract(first_term, second_term):
    """
    Funzione sottrazione
    """
    if(first_term < second_term):
        appoggio = first_term
        first_term = second_term
        second_term = appoggio
        result = float(first_term) - float(second_term)
        return "-" + str(result)

    result = float(first_term) - float(second_term)
    return str(result)


def multiplication(first_term, second_term):
    """
    Funzione moltiplicazione
    """
    result = float(first_term) * float(second_term)
    return str(result)


def division(first_term, second_term):
    """
    Funzione divisione
    """
    if float(second_term) == 0:
        print("Non puoi dividere un numero per zero!")
        return "Err"
    result = float(first_term) / float(second_term)
    return str(result)
