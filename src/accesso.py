"""
Funzione menu, selezione registrazione o login
"""

status = ""


def menu():
    """
    Funzione menu.
    Interfaccia che consente di selezionare se fare login per un utente
    già registrato (n) oppure fare la registrazione per un nuovo utente
    (y).
    L'utente premerà q per uscire dal programma.
    """
    status = input("Sei un nuovo utente? y/n  || Premi q per chiudere --> ")
    if status == "y":
        return "y"
    elif status == "n":
        return "n"
    elif status == "q":
        return "q"
    else:
        print("Input non riconosciuto.")
        return "q"


def input_email():
    """
    Funzione input email
    """
    email = input("Inserisci e-mail:\t")
    return email


def input_password():
    """
    Funzione input password
    """
    password = input("Inserisci password:\t")
    return password
