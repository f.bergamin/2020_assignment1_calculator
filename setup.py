import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="calculator-project",  
    version="0.0.9",
    author="Bergamin,Meroni,Gabrieli",
    author_email="f.bergamin@campus.unimib.it,a.meroni19@campus.unimib.it,c.gabrieli1@campus.unimib.it",
    description="A small calculator package",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/f.bergamin/2020_assignment1_calculator",
    packages=setuptools.find_packages(),
    scripts=['src/accesso.py','src/app.py','src/db.py','src/calcolatrice.py',],
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
