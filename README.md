# Progetto e sviluppo SW Assignment 1

## Link repository
*Repository precedente:* https://gitlab.com/alemero1993/progetto-e-sviluppo-software (minuti esauriti)<br>
*Repository finale:* https://gitlab.com/f.bergamin/2020_assignment1_calculator 

## Membri del gruppo:
   - Federico Bergamin 749676
   - Chiara Gabrieli 858664
   - Alessio Meroni 797784

## Applicazione
L'applicazione consiste in una libreria, scritta in Python, di funzioni matematiche basilari che possono essere utilizzate, attraverso un'interfaccia che sfrutta linee di comando, a seguito di un accesso tramite le credenziali username e password o di una registrazione per un nuovo utente. Il database contenente le credenziali degli utenti è un database di tipo relazionale e viene interrogato tramite linguaggio SQLite.

## Stage DevOps
- **build**: <br>
per gestire facilmente le dipendenze dell'applicazione, viene creata un'immagine con un _Docker_ Container. Il container si basa su un'immagine di Python 3.7 e ad essa vengono aggiunte le dipendenze dell'applicazione. Se l'operazione va a buon fine l'immagine creata viene salvata nella variabile $CONTAINER_IMAGE e utilizzata nei successivi step della pipeline.
- **verify**: <br>
la libreria _flake8_ di Python verifica la presenza di errori nel codice Python e promuove buone prassi di scrittura del codice.
- **unit-test**: <br>
tramite la libreria _pytest_ si avviano le funzioni di testing, contenute nelle directory fornite, create per verificare che le funzioni matematiche dell'applicazione (test_operazioni.py) e le operazioni create per gestire il db degli utenti (test_db.py) funzionino a dovere. 
   * **test_operazioni.py**: 
      + _test_somma()_: controlla la correttezza dell'operazione di somma
      + _test_sottrazione()_: controlla la correttezza dell'operazione di sottrazione
      + _test_moltiplicazione()_: controlla la correttezza dell'operazione di moltiplicazione
      + _test_divisione()_: controlla la correttezza dell'operazione di divisione
   * **test_db.py**:
      + _test_connessione()_: Verifica che la connessione col database sia avvenuta con successivo
      + _test_create_db()_: Verifica che la tabella user_list.db sia stata creata. 
      + _test_insert()_: Si assicura che la l'inserimento del record sia avvenuto con successo.
      + _test_select()_: Verifica che sia selezionato il record corretto.
      + _test_delete()_: Si assicura che il record selezionato venga cancellato con successo.
- **integration-test**: <br>
analogamente allo stage precedente, tramite la libreria _pytest_ si verificano le operazioni di gestione degli utenti(test_app.py).
   * **test_login()**: Vengono effettuati due test. Nel primo si verifica che un login con le giuste credenziali vada a buon fine e nel secondo che tramite credenziali errate il login non abbia successo.
   * **test_registrazione()**: Effettua un test in cui verifica che la registrazione sia andata a buon fine.
   * **test duplicato()**: Controlla che se in fase di registrazione l'utente è già registrato, non permette la registrazione.
- **package**: <br>
crea un pacchetto composto dai file del programma tramite _setuptools_. Il suo artefatto verrà poi mandato al passo successivo della pipeline.
- **release**: <br>
carica il pacchetto generato dallo step precedente su TestPyPi tramite _twine_. Il comando skippa il caricamento del file se la versione è già presente. In tal caso, se si vuole verificare il funzionamento effettivo della fase, è necessario aggiornare il numero relativo alla versione nei file _setup.py_ e _gitlab-ci.yml_ (in relazione alla fase di deploy).
- **deploy**: <br>
si connette ad una VM Azure (IP: 52.149.68.33) tramite ssh, la cui Private Key è passata come variabile privata, e verifica che il pacchetto venga installato correttamente in ambiente Python, in cui la dipendenza con la libreria _py3-validate-email_ è stata installata in precedenza. Abbiamo scelto di non eseguire il programma sulla VM in quanto non è possibile interagire e fornire un input durante l'esecuzione, che genererebbe un EOF Error. Abbiamo verificato la corretta installazione in locale (riferito alla VM) del pacchetto e degli script del programma, che si possono trovare all'interno della directory "_cd /usr/local/bin_". (N.B. E' possibile che la fase fallisca se il tempo che intercorre tra la replease e il deploy non è abbastanza lungo. Rilanciando una seconda volta questa fase funzionerà in maniera corretta e porterà a termine la pipeline con successo.)

## Funzionamento del programma
Il programma consiste nell'esecuzione del file _app.py_. Tramite delle interazioni con l'untente si potrà:
- registrare un nuovo utente, la cui mail verrà controllata nel file stesso tramite la libreria _py3-validate-email_ prima di inserire il nuovo utente nel db.
- fare il login per visualizzare le operazioni della calcolatrice, controllando che l'utente abbia inserito le credenziali corrette.
- eseguire operazioni matematiche. In particolare, chiede che operazione eseguire e poi due numeri in input, i quali possono anche essere negativi o decimali, e restituisce il risultato dell'operazione scelta. Se gli input non sono validi chiede di reinserire il numero. Le operazioni eseguibili sono:
   * _add(a,b)_
   * _subtract(a,b)_
   * _multiplication(a,b)_
   * _division(a,b)_

Il database _users.db_ è un database locale ed è composto da una semplice tabella chiamata _user\_list_ contente due colonne: 
- _email_ (primary key)
- _password_




