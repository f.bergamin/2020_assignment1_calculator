"""
Test operazioni registrazione e login
"""
import sys
sys.path.insert(0, "/builds/f.bergamin/2020_assignment1_calculator/src")
import app  # noqa: E402
import db  # noqa: E402


def test_registrazione():
    """
    test registrazione
    """
    assert app.registrazione("prova12345", "3456")
    db.delete_user("prova12345", "3456")


def test_duplicato():
    """
    test verifica duplicato
    """
    app.registrazione("prova12345", "3456")
    assert not app.registrazione("prova12345", "3456")
    db.delete_user("prova12345", "3456")


def test_login():
    """
    test login
    """
    assert app.login("prova12345", "3456")
    assert not app.login("prova1234", "34567")
