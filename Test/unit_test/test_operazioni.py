"""
test pipeline
"""
import sys
sys.path.insert(0, "/builds/f.bergamin/2020_assignment1_calculator/src")
import calcolatrice as c  # noqa: E402


def test_somma():
    """
    test per la somma
    """
    assert c.add(2, 2) == "4.0"


def test_sottrazione():
    """
    test per la sottrazione
    """
    assert c.subtract(4, 2) == "2.0"
    assert c.subtract(2, 4) == "-2.0"


def test_moltiplicazione():
    """
    test per la moltiplicazione
    """
    assert c.multiplication(4, 2) == "8.0"


def test_divisione():
    """
    test per la divisione
    """
    assert c.division(4, 2) == "2.0"
    assert c.division(2, 0) == "Err"
