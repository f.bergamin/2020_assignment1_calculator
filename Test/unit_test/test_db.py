"""
Test operazioni db
"""
import sys
sys.path.insert(0, "/builds/f.bergamin/2020_assignment1_calculator/src")
import db  # noqa: E402


def test_connessione():
    """
    test connessione database --> OK FUNZIONA
    """
    assert db.connessione()


def test_create_db():
    """
    test creazione tabelle
    """
    assert db.create_db()


def test_insert():
    """
    test insert
    """
    assert db.insert("prova", "prova")
    db.delete_user("prova", "prova")


def test_select():
    """
    test select
    """
    db.insert("prova", "prova")
    assert db.select("prova") == ("prova", "prova")
    db.delete_user("prova", "prova")


def test_delete():
    """
    test eliminazione
    """
    db.insert("prova", "prova")
    assert db.delete_user("prova", "prova")
